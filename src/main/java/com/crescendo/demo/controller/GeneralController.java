package com.crescendo.demo.controller;

import com.crescendo.demo.model.ContactInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
public class GeneralController {

    @Autowired
    MailSender mailSender;

    @RequestMapping("/")
    public String index() {
        return "home";
    }

    @GetMapping("/contactUs")
    public String contactUs(Model model) {
        model.addAttribute("contactInfo", new ContactInfo());
        return "contactUs";
    }

    @PostMapping(value = "/contactUs")
    public String contactUsFormSubmission(@ModelAttribute ContactInfo contactInfo) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setText("Thank you for contacting us, " + contactInfo.getFirstName() + " " + contactInfo.getLastName() + "!");
        message.setTo(contactInfo.getEmail());
        message.setFrom("skillassessment414@gmail.com");
        try {
            mailSender.send(message);
            return "thankYouMessage";
        } catch (Exception e) {
            e.printStackTrace();
            return "emailErrorMessage";
        }
    }
}
